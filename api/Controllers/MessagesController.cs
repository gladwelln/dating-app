using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using api.Data;
using api.Dtos;
using api.Helpers;
using api.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace api.Controllers
{
    [ServiceFilter(typeof(LogUserActivity))]
    [Authorize]
    [Route("api/users/{userId}/[controller]")]
    [ApiController]
    public class MessagesController : ControllerBase
    {
        public IDatingRepository _repo { get; set; }
        public IMapper _mapper { get; set; }
        public MessagesController(IDatingRepository repo, IMapper mapper)
        {
            _mapper = mapper;
            _repo = repo;

        }

        [HttpGet("{id}", Name = "GetMessage")]
        public async Task<IActionResult> GetMessage(int userId, int id)
        {
            if (userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
                return Unauthorized();

            var repoMessage = await _repo.GetMessage(id);
            if (repoMessage == null)
                return NotFound();

            return Ok(repoMessage);
        }

        [HttpGet]
        public async Task<IActionResult> GetUserMessages(int userId, [FromQuery]MessageParams messageParams)
        {
            if (userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
                return Unauthorized();

            messageParams.UserId = userId;
            var repoMessages = await _repo.GetUserMessages(messageParams);
            var messages = _mapper.Map<IEnumerable<MessageReturnDto>>(repoMessages);

            Response.AddPagination(repoMessages.CurrentPage, repoMessages.PageSize, repoMessages.TotalCount, repoMessages.TotalPages);

            return Ok(messages);
        }

        [HttpPost]
        public async Task<IActionResult> CreateMessage(int userId, MessageCreateDto messageCreateDto)
        {
            var sender = await _repo.GetUser(userId);
            if (sender.Id != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
                return Unauthorized();

            messageCreateDto.SenderId = userId;
            var recipient = await _repo.GetUser(messageCreateDto.RecipientId);
            if (recipient == null)
                return BadRequest("Could not find user");

            var message = _mapper.Map<Message>(messageCreateDto);
            _repo.Add(message);

            if (await _repo.SaveAll())
            {
                var returningMessage = _mapper.Map<MessageReturnDto>(message);
                return CreatedAtRoute("GetMessage", new { id = message.Id }, returningMessage);
            }

            throw new Exception("Failed to save message");
        }

        [HttpGet("thread/{recipientId}")]
        public async Task<IActionResult> GetMessageThread(int userId, int recipientId)
        {
            if (userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
                return Unauthorized();

            var repoMessage = await _repo.GetMessageThread(userId, recipientId);
            var messageThread = _mapper.Map<IEnumerable<MessageReturnDto>>(repoMessage);

            return Ok(messageThread);
        }

        [HttpPost("{id}")]
        public async Task<IActionResult> DeleteMessage(int id, int userId)
        {
            if (userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
                return Unauthorized();

            var repoMessage = await _repo.GetMessage(id);
            if (repoMessage.SenderId == userId)
                repoMessage.SenderDeleted = true;

            if (repoMessage.RecipientId == userId)
                repoMessage.RecipientDeleted = true;

            if (repoMessage.SenderDeleted && repoMessage.RecipientDeleted)
                _repo.Delete(repoMessage);

            if (await _repo.SaveAll())
                return NoContent();

            throw new Exception("Could not delete message");
        }

        [HttpPost("{id}/read")]
        public async Task<IActionResult> MarkMessageAsRead(int userId, int id)
        {
            if (userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
                return Unauthorized();

            var message = await _repo.GetMessage(id);
            if (message.RecipientId != userId)
                return Unauthorized();

            message.IsRead = true;
            message.DateRead = DateTime.Now;

            await _repo.SaveAll();

            return NoContent();
        }
    }
}