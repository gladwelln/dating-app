using System.Collections.Generic;
using System.Threading.Tasks;
using api.Helpers;
using api.Models;

namespace api.Data
{
    public interface IDatingRepository
    {
        void Add<T>(T entity) where T : class;

        void Delete<T>(T entity) where T : class;

        Task<bool> SaveAll();

        Task<PageList<User>> GetUsers(UserParams userParams);

        Task<User> GetUser(int id);

        Task<Photo> GetPhoto(int id);
        Task<Photo> GetMainPhoto(int userId);

        Task<Like> GetLike(int userId, int reciientId);

        Task<Message> GetMessage(int id);
        Task<PageList<Message>> GetUserMessages(MessageParams messageParams);
        Task<IEnumerable<Message>> GetMessageThread(int userId, int reciientId);
    }
}